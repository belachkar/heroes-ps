import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { MessagesService } from './messages.service';
import { Hero } from '../models/hero';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private urlHeroes = 'api/heroes';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private msgService: MessagesService
  ) { }

  getHeroes(): Observable<Hero[]> {
    const url = this.urlHeroes;

    return this.http.get<Hero[]>(url)
      .pipe(
        tap(_ => this.log('Fetch heroes')),
        catchError(this.handleErrors('getHeroes', []))
      );
  }

  getHero(id: number): Observable<Hero> {
    const url = `${this.urlHeroes}/${id}`;

    return this.http.get<Hero>(url)
      .pipe(
        tap(_ => this.log(`Fetched hero id=${id}`)),
        catchError(this.handleErrors<Hero>(`getHero id=${id}`))
      );
  }

  searchHeroes(term: string): Observable<Hero[]> {
    const url = `${this.urlHeroes}/?name=${term}`;

    return this.http.get<Hero[]>(url)
      .pipe(
        tap(_ => this.log(`Fetched heroes with name=${term}`)),
        catchError(this.handleErrors('searchHeroes', []))
      );
  }

  updateHero(hero: Hero): Observable<any> {
    const id: number = hero.id;
    const url = `${this.urlHeroes}/${id}`;

    return this.http.put<Hero>(url, hero)
      .pipe(
        tap(_ => this.log(`Updated hero with id=${id}`)),
        catchError(this.handleErrors('updateHero', []))
      );
  }

  addHero(hero: Hero): Observable<any> {
    const url = this.urlHeroes;

    return this.http.post<Hero>(url, hero, this.httpOptions)
      .pipe(
        tap((newHero: Hero) => this.log(`Added hero with id=${newHero.id}`)),
        catchError(this.handleErrors('addHero'))
      );
  }

  deleteHero(hero: Hero): Observable<any> {
    const url = `${this.urlHeroes}/${hero.id}`;

    return this.http.delete(url, this.httpOptions)
      .pipe(
        tap(_ => this.log(`Deleted hero with id=${hero.id}`)),
        catchError(this.handleErrors('addHero'))
      );
  }

  private log(msg: string): void {
    this.msgService.add(msg);
  }

  private handleErrors<T>(operation: string, result?: T) {
    return (error: any) => {
      console.error(error);
      const errMsg = error.message ? error.message : 'Unknown error';
      this.log(`Operation ${operation} failed: ${errMsg}`);
      return of(result as T);
    };
  }

}
