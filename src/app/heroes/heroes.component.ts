import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  title = 'Heroes';
  heroes$: Observable<Hero[]> = of([]);

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroes$ = this.heroService.getHeroes();
  }

  add(heroName: string): void {
    const name = heroName.trim();
    if (!name) { return; }

    this.heroService.addHero({ name } as Hero)
      .subscribe(_ => this.getHeroes());
  }

  delete(hero: Hero): void {
    this.heroService.deleteHero(hero)
      .subscribe(_ => this.getHeroes());
  }

}
