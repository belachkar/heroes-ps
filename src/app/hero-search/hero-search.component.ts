import { Component, OnInit } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Hero } from '../models/hero';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.scss']
})
export class HeroSearchComponent implements OnInit {
  heroes$: Observable<Hero[]> = of([]);
  private searchTerms = new Subject();

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.heroes$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        if (!term) { return of([]); }
        return this.heroService.searchHeroes(term);
      })
    );
  }

  search(term: string) {
    term = term.trim();

    this.searchTerms.next(term);
  }

}
