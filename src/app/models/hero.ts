export interface Hero {
  id: number;
  name: string;
}

export function isHero(hero: Hero): hero is Hero {
  if ((hero as Hero).id) {
    return true;
  }
  return false;
}
